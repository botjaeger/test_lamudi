<?php

namespace Lamudi\TaskManagerBundle\Repository;

/**
 * @todo Create EntityInterface
 */
interface RepositoryInterface
{
    /**
     * Get all entities
     *
     * @return array
     */
    public function all();

    /**
     * Get an entity by id
     *
     * @return EntityInterface
     */
    public function get($id);

    /**
     * Create an entity
     *
     * @param  array  $data
     * @return EntityInterface|false
     */
    public function create($data);

    /**
     * Update an entity
     *
     * @param EntityInterface
     * @param  array  $data
     * @return EntityInterface|false
     */
    public function update($entity, $data);

    /**
     * Persist an entity
     *
     * @param  EntityInterface $entity
     * @return bool
     */
    public function save($entity);
}
