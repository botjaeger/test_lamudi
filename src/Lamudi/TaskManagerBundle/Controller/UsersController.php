<?php

namespace Lamudi\TaskManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Lamudi\TaskManagerBundle\Entity\Users;

class UsersController extends Controller {

    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository('LamudiTaskManagerBundle:Users')
            ->all();

        return $this->render('LamudiTaskManagerBundle:Users:index.html.twig', array(
            "users" => $users,
        ));
    }

    public function showTasksAction(Request $request)
    {
        if ($request->isXMLHttpRequest()) {
        $tasks = $this->getDoctrine()
            ->getRepository('LamudiTaskManagerBundle:Tasks')
            ->getTasksById($request->request->get('value'));

        $data = $this->get('serializer')->serialize($tasks, 'json');
        $response = new JsonResponse($data);
        $response->headers->set('Content-type', 'application/json');
        return $response;
        }
    }

    public function showCategoriesAction(Request $request)
    {
        if ($request->isXMLHttpRequest()) {
            $categories = $this->getDoctrine()
                ->getRepository('LamudiTaskManagerBundle:Category')
                ->getCategoriesById($request->request->get('value'));

            $data = $this->get('serializer')->serialize($categories, 'json');
            $response = new JsonResponse($data);
            $response->headers->set('Content-type', 'application/json');
            return $response;
        }
    }

    public function createTaskAction(Request $request)
    {
        if ($request->isXMLHttpRequest()) {
            $post = array();
            parse_str($request->request->get('value'), $post);
            $repository = $this->getDoctrine()
                ->getRepository('LamudiTaskManagerBundle:Tasks');
            
            $task = $repository->create($post);
                
            if ($repository->save($task) == null) {
                $data = $this->get('serializer')->serialize("success", 'json');
                $response = new JsonResponse($data);
                $response->headers->set('Content-type', 'application/json');
                return $response;
            }
        }
    }

}