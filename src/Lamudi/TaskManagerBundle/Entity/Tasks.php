<?php

namespace Lamudi\TaskManagerBundle\Entity;

/**
 * Tasks
 */
class Tasks
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Lamudi\TaskManagerBundle\Entity\Category
     */
    private $category;

    /**
     * @var \Lamudi\TaskManagerBundle\Entity\Status
     */
    private $status;

    /**
     * @var \Lamudi\TaskManagerBundle\Entity\Users
     */
    private $users;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Tasks
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set category
     *
     * @param \Lamudi\TaskManagerBundle\Entity\Category $category
     *
     * @return Tasks
     */
    public function setCategory(\Lamudi\TaskManagerBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Lamudi\TaskManagerBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set status
     *
     * @param \Lamudi\TaskManagerBundle\Entity\Status $status
     *
     * @return Tasks
     */
    public function setStatus(\Lamudi\TaskManagerBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Lamudi\TaskManagerBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set users
     *
     * @param \Lamudi\TaskManagerBundle\Entity\Users $users
     *
     * @return Tasks
     */
    public function setUsers(\Lamudi\TaskManagerBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \Lamudi\TaskManagerBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }
}

