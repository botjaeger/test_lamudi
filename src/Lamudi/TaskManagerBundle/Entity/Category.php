<?php

namespace Lamudi\TaskManagerBundle\Entity;

/**
 * Category
 */
class Category
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $category_name;

    /**
     * @var \Lamudi\TaskManagerBundle\Entity\UserType
     */
    private $usertype;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     *
     * @return Category
     */
    public function setCategoryName($categoryName)
    {
        $this->category_name = $categoryName;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * Set usertype
     *
     * @param \Lamudi\TaskManagerBundle\Entity\UserType $usertype
     *
     * @return Category
     */
    public function setUsertype(\Lamudi\TaskManagerBundle\Entity\UserType $usertype = null)
    {
        $this->usertype = $usertype;

        return $this;
    }

    /**
     * Get usertype
     *
     * @return \Lamudi\TaskManagerBundle\Entity\UserType
     */
    public function getUsertype()
    {
        return $this->usertype;
    }
}

