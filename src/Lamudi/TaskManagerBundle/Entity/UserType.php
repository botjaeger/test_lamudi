<?php

namespace Lamudi\TaskManagerBundle\Entity;

/**
 * UserType
 */
class UserType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type_name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     *
     * @return UserType
     */
    public function setTypeName($typeName)
    {
        $this->type_name = $typeName;

        return $this;
    }

    /**
     * Get typeName
     *
     * @return string
     */
    public function getTypeName()
    {
        return $this->type_name;
    }
}

