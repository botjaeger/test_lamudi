<?php

namespace Lamudi\TaskManagerBundle\Entity;

/**
 * Users
 */
class Users
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $user_name;

    /**
     * @var \Lamudi\TaskManagerBundle\Entity\UserType
     */
    private $usertype;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return Users
     */
    public function setUserName($userName)
    {
        $this->user_name = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set usertype
     *
     * @param \Lamudi\TaskManagerBundle\Entity\UserType $usertype
     *
     * @return Users
     */
    public function setUsertype(\Lamudi\TaskManagerBundle\Entity\UserType $usertype = null)
    {
        $this->usertype = $usertype;

        return $this;
    }

    /**
     * Get usertype
     *
     * @return \Lamudi\TaskManagerBundle\Entity\UserType
     */
    public function getUsertype()
    {
        return $this->usertype;
    }
}

