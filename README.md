test_lamudi
===========

A Symfony project created on June 19, 2017, 5:43 pm.

use terminal to execute these commands

composer install

go to app/config/parameters.yml set values

database_host: 127.0.0.1

database_port: 3306

database_name: lamudi_taskmanager

database_user: root

database_password: 

use terminal to execute these commands

php bin/console doctrine:generate:entities LamudiTaskManagerBundle --no-backup

php bin/console doctrine:schema:validate

php bin/console doctrine:schema:update --dump-sql --force

or import sql file in db folder

php bin/console server:run